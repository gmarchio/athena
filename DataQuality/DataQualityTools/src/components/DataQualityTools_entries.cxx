#include "DataQualityTools/DQTBackgroundMon.h"
#include "DataQualityTools/DQTLumiMonAlg.h"
#include "DataQualityTools/DQTDataFlowMonAlg.h"
#include "DataQualityTools/DQTDetSynchMonAlg.h"
#include "DataQualityTools/DQTGlobalWZFinderAlg.h"

DECLARE_COMPONENT( DQTBackgroundMon )
DECLARE_COMPONENT( DQTLumiMonAlg )
DECLARE_COMPONENT( DQTDataFlowMonAlg )
DECLARE_COMPONENT( DQTDetSynchMonAlg )
DECLARE_COMPONENT( DQTGlobalWZFinderAlg )

