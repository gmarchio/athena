/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "AthenaPoolExampleData/ExampleElectronContainer.h"
#include "xAODCore/AddDVProxy.h"

ADD_NS_DV_PROXY(xAOD, ExampleElectronContainer);
