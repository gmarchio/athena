/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ACTSTRKEVENT_TRACKPARAMETERS_H
#define ACTSTRKEVENT_TRACKPARAMETERS_H 1

#include "Acts/EventData/TrackParameters.hpp"

// Set up a CLID for the type:
#include "AthenaKernel/CLASS_DEF.h"
CLASS_DEF( Acts::BoundTrackParameters, 207122290, 1 )

#endif
