/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../MmFastDigiTool.h"
#include "../MM_DigitizationTool.h"
DECLARE_COMPONENT(MuonR4::MmFastDigiTool)
DECLARE_COMPONENT(MuonR4::MM_DigitizationTool)
